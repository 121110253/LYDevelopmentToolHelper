//
//  LYAppDelegate.h
//  LYDevelopmentToolHelper
//
//  Created by home10 on 04/14/2019.
//  Copyright (c) 2019 home10. All rights reserved.
//

@import UIKit;

@interface LYAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
